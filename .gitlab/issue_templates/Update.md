# Update
*This template is for requesting upgrades to a component(s) installed on the Docker container produced by this project's GitLab CI pipeline.*

## Which component(s) needs to be updated?
*Use checkboxes* `[ ]` *to list multiple items if necessary.*


## What changes are introduced by the update(s)?
*Include links to release notes. If there are no release notes, please provide a brief summary of the changes.*


## Development plan
*This section should only be filled out by the developer(s). Divide your work up into one or more batches (smaller portions) and list them below. Assign each batch to a person by @mentioning their name.*

*Each batch should have its own merge request. Don't close this issue until all batches are done.*

| Batch | Owner |
| ------ | ------ |
| do this thing | @username |

/label ~"Category: Update" ~"Severity: 2"
/milestone %Backlog
