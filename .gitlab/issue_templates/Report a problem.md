# Report a Problem

## What is the severity of this problem?
* **Low**: Low impact with easy workarounds.
* **Medium**: Medium impact with temporary workarounds.
* **High**: High impact with no viable workarounds.


## Which component(s) is experiencing the problem?


## Expected Behavior


## Actual Behavior
*Please include log output or a link to a GitLab CI job if possible.*


## Development plan
*This section should only be filled out by the developer(s). Divide your work up into one or more batches (smaller portions) and list them below. Assign each batch to a person by @mentioning their name.*

*Each batch should have its own merge request. Don't close this issue until all batches are done.*

| Batch | Owner |
| ------ | ------ |
| do this thing | @username |

/label ~"Category: Bug" ~"Severity: 2"
/milestone %Backlog
