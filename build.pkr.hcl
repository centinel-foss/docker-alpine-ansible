# Build Settings
# ==============
# This file supplies Packer with the configuration
# settings that it needs to provision the Docker
# image that is based on the image specified in
# sources.pkr.hcl.
#
# Packer doesn't support conditional logic, so there
# are different build files for different circumstances.
#
# This one is supposed to run for commits merged to the
# master branch.

build {
  # Pull in the build artifact from the sources
  # listed below.
  sources = [
    "source.docker.docker_container"
  ]

  # Runs a shell script on the Docker container
  # to provision it.
  provisioner "shell" {
    script = "provisioner_shell/provision.sh"
    environment_vars = [
      "ANSIBLE_VERSION=${var.python_ansible_version}",
      "DOCKER_VERSION=${var.python_docker_version}",
      "MOLECULE_VERSION=${var.python_molecule_version}",
      "MOLECULE_DOCKER_VERSION=${var.python_molecule_docker_version}",
      "TOX_VERSION=${var.python_tox_version}"
    ]
  }

  post-processors {
    # Tags the built Docker container appropriately.
    post-processor "docker-tag" {
      repository = "${var.docker_repository}"
      tags = ["${var.docker_tag}"]
    }

    post-processor "docker-push" {
      # Delete the Docker image that was built after
      # it's pushed to the remote registry.
      keep_input_artifact = false
    }
  }

}
